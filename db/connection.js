const { Sequelize } = require("sequelize");

const dbConexion = new Sequelize(
  process.env.DB_DATABASE,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT) || 5433,
    dialect: "postgres",
    logging: false,
  }
);

module.exports = {
  dbConexion,
};

