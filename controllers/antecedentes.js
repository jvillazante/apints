const { response } = require("express");
const { Antecedente } = require("../models/antecedente");

//obtener diagnosticos
const obtenerAntecedentes = async (req, res = response) => {
  const { page = 0, limit = 100 } = req.query;
  const { paciente_id } = req.params;
  const { count, rows } = await Antecedente.findAndCountAll({
    // include: [
    //   {
    //     model: Paciente,
    //   },
    // ],
    where: { paciente_cronico_id: paciente_id, estado_id: 1 },
    // where: {
    //   [Op.and]: [{ paciente_cronico_id: paciente_id }, { estado_id: 1 }],
    // },
    limit,
    offset: page,
    order: [["id", "DESC"]],
  });

  res.json({
    total: count,
    antecedentes: rows,
  });
};

//obtener antecdeentres establecimiento
const obtenerAntecedentesEstablecimiento = async (req, res = response) => {
  const { page = 0, limit = 10 } = req.query;
  const { establecimiento_id } = req.params;

  const antecedentes = await Antecedente.findAndCountAll({
    where: {
      estado_id: 1,
      establecimiento_id,
    },
    limit,
    offset: page,
    order: [["id", "ASC"]],
  });

  res.json({
    total: antecedentes.count,
    antecedentes: antecedentes.rows,
  });
};

//crear paciente
const crearAntecedente = async (req, res = response) => {
  const {
    paciente_cronico_id,
    persona_id,
    actividad_fisica,
    tabaquismo,
    alcoholismo,
    talla,
    peso,
    sistolica,
    diastolica,
    examen_ojo,
    cintura,
    imc_valor,
    imc_descripcion,
    fecha_registro,
    user_id,
    establecimiento_id,
    frutas_verduras,
    sal,
    alimentos_procesados,
    bebidas_azucaradas,
    examen_pie,
    estado_id,
    tipo_g,
  } = req.body;

  //genrar data a guaradr
  const data = {
    paciente_cronico_id,
    persona_id,
    actividad_fisica,
    tabaquismo,
    alcoholismo,
    talla,
    peso,
    sistolica,
    diastolica,
    examen_ojo,
    cintura,
    imc_valor,
    imc_descripcion,
    fecha_registro,
    user_id,
    establecimiento_id,
    frutas_verduras,
    sal,
    alimentos_procesados,
    bebidas_azucaradas,
    examen_pie,
    estado_id,
    tipo_g,
  };
  // console.log(data);
  const antecedente = new Antecedente(data);
  await antecedente.save();

  res.status(201).json(antecedente);
};

module.exports = {
  obtenerAntecedentes,
  crearAntecedente,
  obtenerAntecedentesEstablecimiento,
};
