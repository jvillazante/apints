const { response } = require("express");
const { Variable } = require("../models/variable");
const { QueryTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");

//obtener todas las variables
const obtenerVariables = async (req, res = response) => {
  const variables = await Variable.findAll({
    where: { estado_id: true },
    order: [["descripcion", "ASC"]],
  });

  res.json({
    variables,
  });
};

//obtiene variables (enfermedad) por id paciente
const obtenerVariablesIdPaciente = async (req, res = response) => {
  const { paciente_id } = req.params;
  const variables = await dbConexion.query(
    "SELECT DISTINCT v.descripcion, v.id uid  FROM diagnosticos_cronicos dc, variables v WHERE dc.paciente_cronico_id = :paciente AND dc.estado_id=1 AND v.id=dc.variable_id AND v.estado_id=true",
    {
      replacements: { paciente: paciente_id },
      type: QueryTypes.SELECT,
    }
  );
  res.json({
    variables,
  });
};

module.exports = {
  obtenerVariables,
  obtenerVariablesIdPaciente,
};
