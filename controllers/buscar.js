const { response } = require("express");
const { Op } = require("sequelize");
const { User } = require("../models/user");
const { Cie10 } = require("../models/cie10");

const coleccionesPermitidas = ["users", "cie10"];

const buscarUsers = async (termino = "", res = response) => {
  //   const regex = new RegExp(termino, "i");
  const user = await User.findAll({
    where: { username: { [Op.like]: `%${termino}%` } },
  });
  if (user) {
    return res.json({ results: user ? [user] : [] });
  }
};

const buscarCie10 = async (termino = "", res = response) => {
  const cie10 = await Cie10.findAll({
    where: {
      [Op.or]: [
        { cie_alfa: { [Op.like]: `%${termino}%` } },
        { cie_descripcion: { [Op.like]: `%${termino}%` } },
      ],
    },
  });
  if (cie10) {
    return res.json({ results: cie10 ? [cie10] : [] });
  }
};

const buscar = (req, res = response) => {
  const { coleccion, termino } = req.params;
  //   const termino = req.params.termino.toUpperCase();
  if (!coleccionesPermitidas.includes(coleccion)) {
    return res.status(400).json({
      msg: `Las colecciones permitidas son: ${coleecionesPermitidas}`,
    });
  }

  switch (coleccion) {
    case "cie10":
      buscarCie10(termino, res);
      break;
    case "users":
      buscarUsers(termino, res);
      break;
    default:
      res.status(500).json({
        msg: "No se puede realizar esa busaqueda",
      });
  }

  //   res.json({
  //     coleccion,
  //     termino,
  //   });
};

module.exports = {
  buscar,
};
