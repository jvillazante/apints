const { response } = require("express");
const { Laboratorio } = require("../models/laboratorio");
const { LaboratorioCronico } = require("../models/laboratorioCronico");

const obtenerLaboratoriosCronicosIdPaciente = async (req, res = response) => {
  const { page = 0, limit = 100 } = req.query;
  const { paciente_id } = req.params;
  const { count, rows } = await LaboratorioCronico.findAndCountAll({
    include: [
      {
        model: Laboratorio,
        attributes: [
          "descripcion",
          "unidad",
          "tipo_descripcion",
          "minimo",
          "maximo",
        ],
      },
    ],
    where: {
      estado_id: 1,
      paciente_cronico_id: paciente_id,
    },
    limit,
    offset: page,
    order: [["id", "DESC"]],
  });
  res.json({
    total: count,
    laboratoriosCronicos: rows,
  });
};

const crearLaboratoriosCronico = async (req, res = response) => {
  const {
    paciente_cronico_id,
    persona_id,
    laboratorio_id,
    valor,
    fecha_registro,
    user_id,
    tipo_g,
  } = req.body;

  //genrar data a guaradr
  const data = {
    paciente_cronico_id,
    persona_id,
    laboratorio_id,
    valor,
    fecha_registro,
    user_id,
    tipo_g,
  };

  const laboCroni = new LaboratorioCronico(data);
  await laboCroni.save();

  laboCroniComple = await LaboratorioCronico.findByPk(laboCroni.id, {
    include: [
      {
        model: Laboratorio,
        attributes: [
          "descripcion",
          "unidad",
          "tipo_descripcion",
          "minimo",
          "maximo",
        ],
      },
    ],
  });

  res.status(201).json(laboCroniComple);
};

module.exports = {
  obtenerLaboratoriosCronicosIdPaciente,
  crearLaboratoriosCronico,
};
