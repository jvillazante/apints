const { response } = require("express");
const { Catalogo } = require("../models/catalogo");

//obtener catalgoos por id_tabla
const obtenerCatalogoIdTabla = async (req, res = response) => {
  const { tabla_id } = req.params;
  const catalogos = await Catalogo.findAll({
    where: { catalogovigente: 1, tabla_id },
    order: [["catalogoid", "ASC"]],
  });

  res.json({
    catalogos,
  });
};

module.exports = {
  obtenerCatalogoIdTabla,
};
