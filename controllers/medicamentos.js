const { response } = require("express");
const { QueryTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");
const { Medicamento } = require("../models/medicamento");

//obtiene todos los medicamentos
const obtenerMedicamentos = async (req, res = response) => {
  // const { page = 0, limit = 10 } = req.query;
  const medicamentos = await Medicamento.findAll({
    where: { estado_id: 1 },
    order: [["med_comercial", "ASC"]],
  });

  res.json({
    medicamentos,
  });
};

//obtiene variables (enfermedad) por id paciente
const obtenerMedicamentosIdVariable = async (req, res = response) => {
  const { variable_id } = req.params;
  const medicamentos = await dbConexion.query(
    "SELECT m.* FROM medicamento_ent me, medicamento m WHERE me.medicamento_id = m.id AND me.grupo_ent= :grupo AND m.estado_id=1 AND me.estado_id=1",
    {
      replacements: { grupo: variable_id },
      type: QueryTypes.SELECT,
    }
  );
  res.json({
    medicamentos,
  });
};

module.exports = {
  obtenerMedicamentos,
  obtenerMedicamentosIdVariable,
};
