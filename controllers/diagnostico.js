const { response } = require("express");
const { Cie10 } = require("../models/cie10");
const { Diagnostico } = require("../models/diagnostico");
const { Paciente } = require("../models/paciente");
const { Variable } = require("../models/variable");
// const moment = require("moment");

//obtener diagnosticos
const obtenerDiagnosticos = async (req, res = response) => {
  const { page = 0, limit = 10 } = req.query;
  const { count, rows } = await Diagnostico.findAndCountAll({
    include: [
      {
        model: Variable,
        attributes: ["descripcion"],
      },
      {
        model: Cie10,
        attributes: ["cie_alfa", "cie_descripcion"],
      },
      {
        model: Paciente,
      },
    ],
    where: { estado_id: 1 },
    limit,
    offset: page,
    order: [["id", "DESC"]],
  });

  res.json({
    total: count,
    diagnosticos: rows,
  });
};

//obtener diagnosticos por idPaciente
const obtenerDiagnosticosIdPaciente = async (req, res = response) => {
  const { idPaciente } = req.params;
  // console.log(idPaciente);
  const { count, rows } = await Diagnostico.findAndCountAll({
    where: { estado_id: 1, paciente_cronico_id: idPaciente },
    order: [["id", "DESC"]],
    // include: {
    //   model: Variable,
    //   as: "variable",
    // },
  });

  res.json({
    total: count,
    diagnosticos: rows,
  });
};

//crear paciente
const crearDiagnostico = async (req, res = response) => {
  //generar data a guaradr
  const {
    paciente_cronico_id,
    variable_id,
    diagnostico_id,
    fecha_registro,
    sedes_id,
    red_id,
    municipio_id,
    establecimiento_id,
    tipo_registro,
  } = req.body;
  const data = {
    paciente_cronico_id,
    persona_id: 1,
    variable_id,
    diagnostico_id,
    fecha_registro,
    sedes_id,
    red_id,
    municipio_id,
    establecimiento_id,
    tipo_registro,
    user_id: req.user.id,
    estado_id: 1,
  };
  // console.log(data);

  const diagnostico = new Diagnostico(data);

  //guardar DB
  await diagnostico.save();

  res.status(201).json(diagnostico);
};

//actualziar diagnostico

//borrar diagnostico

module.exports = {
  obtenerDiagnosticos,
  obtenerDiagnosticosIdPaciente,
  crearDiagnostico,
};
