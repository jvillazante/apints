const { response, request } = require("express");
const bcryptjs = require("bcryptjs");

const { User } = require("../models/user");

const usersget = async (req = request, res = response) => {
  const { page = 0, limit = 10 } = req.query;
  const { count, rows } = await User.findAndCountAll({
    where: { status: true },
    limit,
    offset: page,
    order: [["id", "ASC"]],
  });

  res.json({
    total: count,
    users: rows,
  });
};

const userPost = async (req, res = response) => {
  const {
    password,
    departamento_id,
    red_id,
    municipio_id,
    establecimiento_id,
  } = req.body;
  const username = req.body.username.toUpperCase();
  const user = new User({
    username,
    password,
    departamento_id,
    red_id,
    municipio_id,
    establecimiento_id,
  });
  const salt = bcryptjs.genSaltSync();
  user.password = bcryptjs.hashSync(password, salt);
  await user.save();
  res.json(user);
};
const userPut = async (req, res = response) => {
  const { id } = req.params;
  const { password, username } = req.body;

  const user = await User.findByPk(id);
  if (password) {
    const salt = bcryptjs.genSaltSync();
    user.password = bcryptjs.hashSync(password, salt);
  }
  user.username = username.toUpperCase();
  await user.save();
  res.json(user);
};

const userDelete = async (req, res = response) => {
  const { id } = req.params;
  // const uid = req.uid;
  const user = await User.findByPk(id);
  user.status = false;
  await user.save();
  res.json(user);
};

module.exports = {
  usersget,
  userPost,
  userPut,
  userDelete,
};
