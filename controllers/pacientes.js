const { response } = require("express");
const { Paciente } = require("../models/paciente");
const moment = require("moment");

//obtener pacientes
const obtenerPacientes = async (req, res = response) => {
  const { page = 0, limit = 10 } = req.query;
  const { sedes, red, municipio, establecimiento } = req.params;
  // console.log(sedes, red, municipio, establecimiento);
  // const { sedes_id, red_id, municipio_id, establecimiento_id } = req.body;
  const { count, rows } = await Paciente.findAndCountAll({
    where: {
      estado_id: 1,
      sedes_id: sedes,
      red_id: red,
      municipio_id: municipio,
      establecimiento_id: establecimiento,
    },
    limit,
    offset: page,
    order: [["id", "ASC"]],
  });

  res.json({
    total: count,
    pacientes: rows,
  });
};

//obtener paciente
const obtenerPaciente = async (req, res = response) => {
  const { id } = req.params;
  const paciente = await Paciente.findByPk(id);
  res.json(paciente);
};

//crear paciente
const crearPaciente = async (req, res = response) => {
  const nombres = req.body.nombres.toUpperCase();
  const ci = req.body.nrodocumento.toUpperCase();
  const fecha_nac = req.body.fechanacimiento;
  const primerapellido = req.body.primerapellido.toUpperCase();
  const segundoapellido = req.body.segundoapellido?.toUpperCase();
  const direccion = req.body.direccion.toUpperCase();
  const {
    sedes_id,
    red_id,
    municipio_id,
    establecimiento_id,
    tipodocumento_genericoid,
    complemento,
    esasegurado,
    esverificadosegip,
    esadscrito,
    sexo_genericoid,
    correo_electronico,
    edad_registro,
    persona_id,
  } = req.body;

  const pacienteDB = await Paciente.findOne({
    where: { nrodocumento: ci, fechanacimiento: fecha_nac },
  });

  if (pacienteDB) {
    return res.status(400).json({
      msg: `El paciente con CI: ${ci} ya existe.`,
    });
  }
  //genrar data a guaradr
  const data = {
    sedes_id,
    red_id,
    municipio_id,
    establecimiento_id,
    tipodocumento_genericoid,
    nrodocumento: ci,
    complemento,
    fechanacimiento: fecha_nac,
    esasegurado,
    esverificadosegip,
    esadscrito,
    nombres,
    primerapellido,
    segundoapellido,
    sexo_genericoid,
    correo_electronico,
    celular: req.body.celular,
    edad_registro,
    direccion,
    user_id: req.user.id,
    persona_id,
    estado_id: 1,
    nacionalidad_genericoid: 12,
    fecha_registro: moment(),
  };
  // console.log(data);

  const paciente = new Paciente(data);
  //guardar DB
  await paciente.save();

  res.status(201).json(paciente);
};

//actualziar paciente

//borrar paciente

module.exports = {
  crearPaciente,
  obtenerPacientes,
  obtenerPaciente,
};
