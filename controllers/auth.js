const { response } = require("express");
const bcryptjs = require("bcryptjs");
const { User } = require("../models/user");
const { generaJWT } = require("../helpers/generar-jwt");

const login = async (req, res = response) => {
  const { username, password } = req.body;

  try {
    //verificar si el ussername existe
    const username_May = username.toUpperCase();
    const user = await User.findOne({ where: { username: username_May } });
    // console.log(user);
    if (!user) {
      return res.status(400).json({
        msg: "Usuario / password no son correctos1",
      });
    }
    //si el usuario esta activo
    if (!user.status) {
      return res.status(400).json({
        msg: "Usuario / password no son correctos2",
      });
    }
    //verificar la constraseña
    const validaPassword = bcryptjs.compareSync(password, user.password);
    if (!validaPassword) {
      return res.status(400).json({
        msg: "Usuario / password no son correctos3",
      });
    }

    //generar el JWT
    const token = await generaJWT(user.id);

    res.json({
      user,
      token,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      msg: "Hable con el adminsitrador",
    });
  }
};

const validarTokenUser = async (req, res = response) => {
  // Generar el JWT
  const token = await generaJWT(req.user.id);

  res.json({
    user: req.user,
    token: token,
  });
};

module.exports = {
  login,
  validarTokenUser,
};
