const { response } = require("express");
const { Laboratorio } = require("../models/laboratorio");

const obtenerLaboratorios = async (req, res = response) => {
  const laboratorios = await Laboratorio.findAll({
    order: [["id", "ASC"]],
  });
  res.json({
    laboratorios,
  });
};

module.exports = {
  obtenerLaboratorios,
};
