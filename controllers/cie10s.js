const { response } = require("express");
const { Cie10 } = require("../models/cie10");

//obtener todas las variables
const obtenerCie10s = async (req, res = response) => {
  const cie10s = await Cie10.findAll({
    order: [["cie_alfa", "ASC"]],
  });

  res.json({
    cie10s,
  });
};

module.exports = {
  obtenerCie10s,
};
