const { response } = require("express");
const { Medicamento } = require("../models/medicamento");
const { Tratamiento } = require("../models/tratamiento");
const { Variable } = require("../models/variable");

const obtenerTratamientosIdPaciente = async (req, res = response) => {
  const { page = 0, limit = 100 } = req.query;
  const { paciente_id } = req.params;
  const { count, rows } = await Tratamiento.findAndCountAll({
    include: [
      {
        model: Medicamento,
        attributes: [
          "codificacion",
          "med_comercial",
          "tipo",
          "med_unidad",
          "med_concentracion",
          "med_generico",
        ],
      },
      {
        model: Variable,
        attributes: ["descripcion"],
      },
    ],
    where: {
      estado_id: 1,
      paciente_cronico_id: paciente_id,
    },
    limit,
    offset: page,
    order: [["id", "DESC"]],
  });
  res.json({
    total: count,
    tratamientos: rows,
  });
};

const crearTratamiento = async (req, res = response) => {
  const {
    paciente_cronico_id,
    persona_id,
    enfermedad_id,
    medicamento_id,
    tratamiento,
    fecha_registro,
    user_id,
    tipo_g,
  } = req.body;

  //genrar data a guaradr
  const data = {
    paciente_cronico_id,
    persona_id,
    enfermedad_id,
    medicamento_id,
    tratamiento,
    fecha_registro,
    user_id,
    tipo_g,
  };

  const trata = new Tratamiento(data);
  await trata.save();

  trataComple = await Tratamiento.findByPk(trata.id, {
    include: [
      {
        model: Medicamento,
        // attributes: [
        //   "descripcion",
        //   "unidad",
        //   "tipo_descripcion",
        //   "minimo",
        //   "maximo",
        // ],
      },
      {
        model: Variable,
      },
    ],
  });

  res.status(201).json(trataComple);
};

module.exports = {
  obtenerTratamientosIdPaciente,
  crearTratamiento,
};
