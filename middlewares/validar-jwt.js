const { response, request } = require("express");
const jwt = require("jsonwebtoken");
const { User } = require("../models/user");

const validarJWT = async (req = request, res = response, next) => {
  const token = req.header("x-token");
  if (!token) {
    return res.status(401).json({
      msg: "Token requerido",
    });
  }
  try {
    const { uid } = jwt.verify(token, process.env.SECRETORPRIVATEKEY);
    req.uid = uid;

    user = await User.findByPk(uid);

    //si el usuario noexite
    if (!user) {
      return res.status(401).json({
        msg: "Tokn no valido",
      });
    }

    //verficar si eñ uid tien eestado true
    if (!user.status) {
      return res.status(401).json({
        msg: "Tokn no valido",
      });
    }
    req.user = user;
    next();
  } catch (error) {
    console.log(error);
    res.status(401).json({
      msg: "Token no valido",
    });
  }
};

module.exports = {
  validarJWT,
};
