// const { Paciente, User, Variable, Cie10 } = require("../models");
const { Paciente } = require("../models/paciente");
const { User } = require("../models/user");
const { Variable } = require("../models/variable");
const { Cie10 } = require("../models/cie10");
const { Laboratorio } = require("../models/laboratorio");
const { Medicamento } = require("../models/medicamento");

// const { User } = require("../models/user");
// const { Variable } = require("../models/variable");

const existeUsername = async (username = "") => {
  const username_May = username.toUpperCase();
  const existeUsername = await User.findAll({
    where: { username: username_May },
  });
  // console.log(existeUsername);
  if (existeUsername.length > 0) {
    throw new Error(`El username: ${username} ya se encuentra registrado`);
  }
};

const existeId = async (id = 0) => {
  const existeId = await User.findByPk(id);
  if (!existeId || !existeId.status) {
    throw new Error(`El id: ${id} no es valido.`);
  }
  // if (!existeId.status) {
  //   throw new Error(`El id: ${id} no es valido2.`);
  // }
};

const existePaciente = async (id = 0) => {
  const existePaciente = await Paciente.findByPk(id);
  if (!existePaciente || existePaciente.estado_id == 0) {
    throw new Error(`El id: ${id} no es valido.`);
  }
  // if (!existeId.status) {
  //   throw new Error(`El id: ${id} no es valido2.`);
  // }
};

const existeVariable = async (id = 0) => {
  const existeVariable = await Variable.findByPk(id);
  if (!existeVariable || !existeVariable.estado_id) {
    throw new Error(`El id: ${id} no es valido.`);
  }
};

const existeCie10 = async (cie10 = "") => {
  // console.log(diagnostico_id);
  const existeCie10 = await Cie10.findByPk(cie10);
  // console.log(existeCie10);
  if (!existeCie10) {
    throw new Error(`El codigo cie10: ${cie10} no es valido.`);
  }
};

const existeLaboratorio = async (id = 0) => {
  const existeLaboratorio = await Laboratorio.findByPk(id);
  if (!existeLaboratorio) {
    throw new Error(`El laboratorio con id: ${id} no es valido.`);
  }
};

const existeMedicamento = async (id = 0) => {
  const existeMedi = await Medicamento.findByPk(id);
  if (!existeMedi) {
    throw new Error(`El medicamento con id: ${id} no es valido.`);
  }
};

module.exports = {
  existeUsername,
  existeId,
  existePaciente,
  existeVariable,
  existeCie10,
  existeLaboratorio,
  existeMedicamento,
};
