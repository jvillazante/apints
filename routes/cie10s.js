const { Router } = require("express");
const { validarJWT } = require("../middlewares");
const { obtenerCie10s } = require("../controllers/cie10s");

const router = Router();

// obtener todos los pacientes
router.get("/", [validarJWT], obtenerCie10s);

module.exports = router;
