const { Router } = require("express");
const { obtenerCatalogoIdTabla } = require("../controllers/catalogo");
const { validarJWT } = require("../middlewares");


const router = Router();

router.get("/:tabla_id", [validarJWT], obtenerCatalogoIdTabla);

module.exports = router;
