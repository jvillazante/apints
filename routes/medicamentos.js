const { Router } = require("express");
const { validarJWT, validarCampos } = require("../middlewares");
const { existeVariable } = require("../helpers/db-validators");
const { check } = require("express-validator");
const {
  obtenerMedicamentosIdVariable,
  obtenerMedicamentos,
} = require("../controllers/medicamentos");
const router = Router();

// obtener todas los medicamentos
router.get("/", [validarJWT], obtenerMedicamentos);

// obtenemos todas las variables por Id Paciente(Diagnostico Cronico)
router.get(
  "/:variable_id",
  [validarJWT, check("variable_id").custom(existeVariable), validarCampos],
  obtenerMedicamentosIdVariable
);

module.exports = router;
