const { Router } = require("express");
const { check } = require("express-validator");

const { validarCampos, validarJWT } = require("../middlewares");

const { existeUsername, existeId } = require("../helpers/db-validators");
const {
  usersget,
  userPost,
  userPut,
  userDelete,
} = require("../controllers/users");
const router = Router();

router.get("/", usersget);
router.post(
  "/",
  [
    check("departamento_id", "El departamento es requerido").not().isEmpty(),
    check("red_id", "La red es requerido").not().isEmpty(),
    check("municipio_id", "El municipio es requerido").not().isEmpty(),
    check("establecimiento_id", "El establecimiento es requerido")
      .not()
      .isEmpty(),
    check("username", "El username es requerido").not().isEmpty(),
    check("username").custom(existeUsername),
    check(
      "password",
      "La contraseña es obligatoria y mas de 6 letras"
    ).isLength({ min: 6 }),
    validarCampos,
  ],
  userPost
);
router.put(
  "/:id",
  [
    check("id").custom(existeId),
    check(
      "password",
      "La contraseña es obligatoria y mas de 6 letras"
    ).isLength({ min: 6 }),
    validarCampos,
  ],
  userPut
);
router.delete(
  "/:id",
  [validarJWT, check("id").custom(existeId), validarCampos],
  userDelete
);

module.exports = router;
