const { Router } = require("express");
const { check } = require("express-validator");
const { login, validarTokenUser } = require("../controllers/auth");
const { validarJWT } = require("../middlewares");
const { validarCampos } = require("../middlewares/validar-campos");

const router = Router();

router.post(
  "/login",
  [
    check("username", "El username es obligatorio").not().isEmpty(),
    check("password", "La contraseña es obligatoria").not().isEmpty(),
  ],
  validarCampos,
  login
);

router.get("/", [validarJWT], validarTokenUser);

module.exports = router;
