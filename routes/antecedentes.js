const { Router } = require("express");
const { check } = require("express-validator");
const { validarJWT, validarCampos } = require("../middlewares");
const {
  obtenerAntecedentes,
  crearAntecedente,
  obtenerAntecedentesEstablecimiento,
} = require("../controllers/antecedentes");
const router = Router();

// obtener todos los pacientes
router.get("/:paciente_id", [validarJWT], obtenerAntecedentes);

// obtener todos los pacientes
router.get(
  "/:establecimiento_id/estab",
  [validarJWT],
  obtenerAntecedentesEstablecimiento
);

//crear un antecedente,
router.post(
  "/",
  [
    validarJWT,
    check("paciente_cronico_id", "El paciente es obligatorio").not().isEmpty(),
    check("persona_id", "La persona es obligatorio").not().isEmpty(),
    check("actividad_fisica", "La actividad fisica es obligatorio")
      .not()
      .isEmpty(),
    check("tabaquismo", "El consumo de tabaco es obligatorio").not().isEmpty(),
    check("alcoholismo", "El conusmo de alcohol es obligatorio")
      .not()
      .isEmpty(),
    check("talla", "La talla es obligatorio").not().isEmpty(),
    check("peso", "El peso es obligatorio").not().isEmpty(),
    check("sistolica", "La presion sistolica es obligatorio").not().isEmpty(),
    check("diastolica", "La presion diastolica es obligatorio").not().isEmpty(),
    check("examen_ojo", "El examen de ojos es obligatorio").not().isEmpty(),
    check("cintura", "La circunferencia de la cintura es obligatorio")
      .not()
      .isEmpty(),
    check("imc_valor", "El IMC es obligatorio").not().isEmpty(),
    // check("imc_descripcion", "La descripcion del IMC es obligatorio")
    //   .not()
    //   .isEmpty(),
    check("fecha_registro", "La fecha de registro es obligatorio")
      .not()
      .isEmpty(),
    check("user_id", "El usuario es obligatorio").not().isEmpty(),
    check("establecimiento_id", "El establecimiento es obligatorio")
      .not()
      .isEmpty(),
    check("frutas_verduras", "El cosumo de verduras es obligatorio")
      .not()
      .isEmpty(),
    check("sal", "El cosumo de sal es obligatorio").not().isEmpty(),
    check(
      "alimentos_procesados",
      "El cosumo de alimentos procesados es obligatorio"
    )
      .not()
      .isEmpty(),
    check(
      "bebidas_azucaradas",
      "El cosumo de bebidas azucaradas es obligatorio"
    )
      .not()
      .isEmpty(),
    check("examen_pie", "El examen de pie es obligatorio").not().isEmpty(),
    check("tipo_g", "El tipo de registro es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  crearAntecedente
);

// //actualizar un paciente,
// router.put("/:id", (req, res) => {
//   res.json("put");
// });

// //borrar un paciente,
// router.delete("/:id", (req, res) => {
//   res.json("delete");
// });

module.exports = router;
