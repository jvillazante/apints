const { Router } = require("express");
const { check } = require("express-validator");
const { validarCampos, validarJWT } = require("../middlewares");
const {
  crearPaciente,
  obtenerPacientes,
  obtenerPaciente,
} = require("../controllers/pacientes");
const { existePaciente } = require("../helpers/db-validators");
const router = Router();

// obtener todos los pacientes
router.get(
  "/:sedes/:red/:municipio/:establecimiento",
  [validarJWT],
  obtenerPacientes
);

//obtener paciente por ID
router.get(
  "/:id",
  [validarJWT, check("id").custom(existePaciente), validarCampos],
  obtenerPaciente
);

//crear un paciente,
router.post(
  "/",
  [
    validarJWT,
    check("sedes_id", "El departamento es obligatorio").not().isEmpty(),
    check("red_id", "La red es obligatorio").not().isEmpty(),
    check("municipio_id", "El municipio es obligatorio").not().isEmpty(),
    check("establecimiento_id", "El establecimiento es obligatorio")
      .not()
      .isEmpty(),
    check("nrodocumento", "El nro de documento es obligatorio").not().isEmpty(),
    check("fechanacimiento", "La fecha de nacimiento es obligatorio")
      .not()
      .isEmpty(),
    check("nombres", "El nombre es obligatorio").not().isEmpty(),
    check("primerapellido", "El primer apellido es obligatorio")
      .not()
      .isEmpty(),
    check("sexo_genericoid", "El sexo apellido es obligatorio").not().isEmpty(),
    check("nacionalidad_genericoid", "La nacionalidad es obligatorio")
      .not()
      .isEmpty(),
    check("direccion", "La direccion es obligatorio").not().isEmpty(),
    check("edad_registro", "La eadd es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  crearPaciente
);

//actualizar un paciente,
router.put("/:id", (req, res) => {
  res.json("put");
});

//borrar un paciente,
router.delete("/:id", (req, res) => {
  res.json("delete");
});

module.exports = router;
