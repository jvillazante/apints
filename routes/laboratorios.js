const { Router } = require("express");
const { obtenerLaboratorios } = require("../controllers/laboratorios");
const { validarJWT } = require("../middlewares");

const router = Router();

router.get("/", [validarJWT], obtenerLaboratorios);

module.exports = router;
