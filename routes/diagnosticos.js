const { Router } = require("express");
const { check } = require("express-validator");
const { validarCampos, validarJWT } = require("../middlewares");
const {
  obtenerDiagnosticosIdPaciente,
  crearDiagnostico,
  obtenerDiagnosticos,
} = require("../controllers/diagnostico");

const {
  existePaciente,
  existeCie10,
  existeVariable,
} = require("../helpers/db-validators");
const router = Router();

//obtener todos los diagnosticos
router.get("/", [validarJWT], obtenerDiagnosticos);

//obtener diagnosticos por IdPaciente
router.get(
  "/:idPaciente",
  [validarJWT, check("idPaciente").custom(existePaciente), validarCampos],
  obtenerDiagnosticosIdPaciente
);

//crear diagnostico,
router.post(
  "/",
  [
    validarJWT,
    check("paciente_cronico_id").custom(existePaciente),
    check("variable_id").custom(existeVariable),
    check("diagnostico_id").custom(existeCie10),
    check("fecha_registro", "La fecha de regsitro es obligatorio")
      .not()
      .isEmpty(),
    check("sedes_id", "El departamento es obligatorio").not().isEmpty(),
    check("red_id", "La red es obligatorio").not().isEmpty(),
    check("municipio_id", "El municipio es obligatorio").not().isEmpty(),
    check("establecimiento_id", "El establecimiento es obligatorio")
      .not()
      .isEmpty(),
    check("tipo_registro", "El tipo de resgistro es obligatorio")
      .not()
      .isEmpty(),
    validarCampos,
  ],
  crearDiagnostico
);

//actualizar diagnostico,
router.put("/:id", (req, res) => {
  res.json("put");
});

//borrar un diagnostico,
router.delete("/:id", (req, res) => {
  res.json("delete");
});

module.exports = router;
