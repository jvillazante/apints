const { Router } = require("express");
const { check } = require("express-validator");
const {
  obtenerLaboratoriosCronicosIdPaciente,
  crearLaboratoriosCronico,
} = require("../controllers/laboratoriosCronicos");
const {
  existePaciente,
  existeLaboratorio,
} = require("../helpers/db-validators");
const { validarJWT, validarCampos } = require("../middlewares");

const router = Router();

router.get(
  "/:paciente_id",
  [validarJWT],
  obtenerLaboratoriosCronicosIdPaciente
);

//crear un paciente,
router.post(
  "/",
  [
    validarJWT,
    check("paciente_cronico_id", "El paciente es obligatorio").not().isEmpty(),
    check("paciente_cronico_id").custom(existePaciente),
    check("persona_id", "La persona es obligatorio").not().isEmpty(),
    check("laboratorio_id", "El laboratorio es obligatorio").not().isEmpty(),
    check("laboratorio_id").custom(existeLaboratorio),
    check("valor", "El valor es obligatorio").not().isEmpty(),
    check("fecha_registro", "La fecha de registro es obligatorio")
      .not()
      .isEmpty(),
    check("user_id", "El usuario es obligatorio").not().isEmpty(),
    check("tipo_g", "El tipo de registro es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  crearLaboratoriosCronico
);

module.exports = router;
