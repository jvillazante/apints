const { Router } = require("express");
const { validarJWT, validarCampos } = require("../middlewares");
const {
  obtenerVariables,
  obtenerVariablesIdPaciente,
} = require("../controllers/variables");
const { existePaciente } = require("../helpers/db-validators");
const { check } = require("express-validator");
const router = Router();

// obtener todas las variables
router.get("/", [validarJWT], obtenerVariables);

// obtenemos todas las variables por Id Paciente(Diagnostico Cronico)
router.get(
  "/:paciente_id",
  [validarJWT, check("paciente_id").custom(existePaciente), validarCampos],
  obtenerVariablesIdPaciente
);

module.exports = router;
