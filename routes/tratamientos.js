const { Router } = require("express");
const { check } = require("express-validator");
const {
  obtenerTratamientosIdPaciente,
  crearTratamiento,
} = require("../controllers/tratamientos");
const {
  existePaciente,
  existeMedicamento,
} = require("../helpers/db-validators");
const { validarJWT, validarCampos } = require("../middlewares");

const router = Router();

router.get(
  "/:paciente_id",
  [validarJWT, check("paciente_id").custom(existePaciente), validarCampos],
  obtenerTratamientosIdPaciente
);

//crear un tratamiento,
router.post(
  "/",
  [
    validarJWT,
    check("paciente_cronico_id", "El paciente es obligatorio").not().isEmpty(),
    check("paciente_cronico_id").custom(existePaciente),
    check("persona_id", "La persona es obligatorio").not().isEmpty(),
    check("medicamento_id", "El medicamento es obligatorio").not().isEmpty(),
    check("medicamento_id").custom(existeMedicamento),
    check("tratamiento", "El tratamiento es obligatorio").not().isEmpty(),
    check("fecha_registro", "La fecha de registro es obligatorio")
      .not()
      .isEmpty(),
    check("user_id", "El usuario es obligatorio").not().isEmpty(),
    check("tipo_g", "El tipo de registro es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  crearTratamiento
);

module.exports = router;
