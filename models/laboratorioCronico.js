const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");
const { Laboratorio } = require("./laboratorio");

class LaboratorioCronico extends Model {}

LaboratorioCronico.init(
  {
    // id: {
    //   type: DataTypes.INTEGER,
    //   primaryKey: true,
    // },
    paciente_cronico_id: {
      type: DataTypes.INTEGER,
    },
    persona_id: {
      type: DataTypes.INTEGER,
    },
    laboratorio_id: {
      type: DataTypes.INTEGER,
    },
    valor: {
      type: DataTypes.DOUBLE,
    },
    fecha_registro: {
      type: DataTypes.DATE,
    },
    estado_id: {
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    tipo_g: {
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "laboratorios_cronicos",
    timestamps: false,
    freezeTableName: true,
  }
);

LaboratorioCronico.belongsTo(Laboratorio, { foreignKey: "laboratorio_id" });

LaboratorioCronico.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  delete values.estado_id;
  delete values.laboratorio_id;
  return values;
};

module.exports = {
  LaboratorioCronico,
};
