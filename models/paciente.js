const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");

class Paciente extends Model {}

Paciente.init(
  {
    sedes_id: {
      type: DataTypes.INTEGER,
    },
    red_id: {
      type: DataTypes.INTEGER,
    },
    municipio_id: {
      type: DataTypes.INTEGER,
    },
    establecimiento_id: {
      type: DataTypes.INTEGER,
    },
    estado_id: {
      type: DataTypes.INTEGER,
    },
    tipodocumento_genericoid: {
      type: DataTypes.INTEGER,
    },
    nrodocumento: {
      type: DataTypes.STRING,
    },
    complemento: {
      type: DataTypes.STRING,
    },
    fechanacimiento: {
      type: DataTypes.DATE,
    },
    esasegurado: {
      type: DataTypes.SMALLINT,
    },
    esverificadosegip: {
      type: DataTypes.SMALLINT,
    },
    esadscrito: {
      type: DataTypes.SMALLINT,
    },
    nombres: {
      type: DataTypes.STRING,
    },
    primerapellido: {
      type: DataTypes.STRING,
    },
    segundoapellido: {
      type: DataTypes.STRING,
    },
    sexo_genericoid: {
      type: DataTypes.INTEGER,
    },
    correo_electronico: {
      type: DataTypes.STRING,
    },
    celular: {
      type: DataTypes.STRING,
    },
    fecha_registro: {
      type: DataTypes.DATEONLY,
    },
    edad_registro: {
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    nacionalidad_genericoid: {
      type: DataTypes.INTEGER,
    },
    direccion: {
      type: DataTypes.STRING,
    },
    persona_id: {
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "pacientes_cronicos",
    timestamps: false,
    freezeTableName: true,
  }
);

Paciente.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  delete values.estado_id;
  return values;
};

module.exports = {
  Paciente,
};
