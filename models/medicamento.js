const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");

class Medicamento extends Model {}

Medicamento.init(
  {
    codificacion: {
      type: DataTypes.STRING,
    },
    med_comercial: {
      type: DataTypes.STRING,
    },
    tipo: {
      type: DataTypes.STRING,
    },
    med_unidad: {
      type: DataTypes.STRING,
    },
    med_concentracion: {
      type: DataTypes.STRING,
    },
    med_comercial: {
      type: DataTypes.STRING,
    },
    med_generico: {
      type: DataTypes.STRING,
    },
    estado_id: {
      type: DataTypes.SMALLINT,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "medicamento",
    timestamps: false,
    freezeTableName: true,
  }
);

Medicamento.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  delete values.estado_id;
  return values;
};

module.exports = {
  Medicamento,
};
