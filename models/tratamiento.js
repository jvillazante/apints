const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");
const { Medicamento } = require("./medicamento");
const { Variable } = require("./variable");

class Tratamiento extends Model {}

Tratamiento.init(
  {
    // id: {
    //   type: DataTypes.INTEGER,
    //   primaryKey: true,
    // },
    paciente_cronico_id: {
      type: DataTypes.INTEGER,
    },
    persona_id: {
      type: DataTypes.INTEGER,
    },
    enfermedad_id: {
      type: DataTypes.INTEGER,
    },
    medicamento_id: {
      type: DataTypes.INTEGER,
    },
    tratamiento: {
      type: DataTypes.STRING,
    },
    fecha_registro: {
      type: DataTypes.DATE,
    },
    estado_id: {
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    tipo_g: {
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "tratamientos_pacientes_cronicos",
    timestamps: false,
    freezeTableName: true,
  }
);

Tratamiento.belongsTo(Medicamento, { foreignKey: "medicamento_id" });
Tratamiento.belongsTo(Variable, { foreignKey: "enfermedad_id" });

Tratamiento.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  delete values.estado_id;
  delete values.enfermedad_id;
  delete values.medicamento_id;
  return values;
};

module.exports = {
  Tratamiento,
};
