const express = require("express");
const cors = require("cors");
const { dbConexion } = require("../db/connection");

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;

    this.paths = {
      auth: "/api/auth",
      buscar: "/api/buscar",
      cie10s: "/api/cie10s",
      pacientes: "/api/pacientes",
      diagnosticos: "/api/diagnosticos",
      users: "/api/users",
      variables: "/api/variables",
      antecedentes: "/api/antecedentes",
      catalogos: "/api/catalogos",
      laboratorios: "/api/laboratorios",
      laboratoriosCronicos: "/api/laboratorios-cronicos",
      // medicamentosEnt: "/api/medicamentos-ent",
      medicamentos: "/api/medicamentos",
      tratamientos: "/api/tratamientos",
    };

    //conectar a base de datos
    this.conectarDB();

    //middlewares
    this.middlewares();

    //rutas
    this.routes();
  }

  async conectarDB() {
    await dbConexion.authenticate();
  }

  middlewares() {
    //cors
    this.app.use(cors());

    //parseo y lectura de body
    this.app.use(express.json());

    //diredctorio publico
    this.app.use(express.static("public"));
  }

  routes() {
    this.app.use(this.paths.auth, require("../routes/auth"));
    this.app.use(this.paths.buscar, require("../routes/buscar"));
    this.app.use(this.paths.cie10s, require("../routes/cie10s"));
    this.app.use(this.paths.pacientes, require("../routes/pacientes"));
    this.app.use(this.paths.diagnosticos, require("../routes/diagnosticos"));
    this.app.use(this.paths.users, require("../routes/users"));
    this.app.use(this.paths.variables, require("../routes/variables"));
    this.app.use(this.paths.antecedentes, require("../routes/antecedentes"));
    this.app.use(this.paths.catalogos, require("../routes/catalogos"));
    this.app.use(this.paths.laboratorios, require("../routes/laboratorios"));
    this.app.use(
      this.paths.laboratoriosCronicos,
      require("../routes/laboratoriosCronicos")
    );
    this.app.use(this.paths.medicamentos, require("../routes/medicamentos"));
    this.app.use(this.paths.tratamientos, require("../routes/tratamientos"));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log("Servidor corriendo en el puerto ", this.port);
    });
  }
}

module.exports = Server;
