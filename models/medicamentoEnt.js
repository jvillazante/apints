const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");
const { Medicamento } = require("./medicamento");
const { Variable } = require("./variable");

class medicamentoEnt extends Model {}

medicamentoEnt.init(
  {
    // id: {
    //   type: DataTypes.INTEGER,
    //   primaryKey: true,
    // },
    medicamento_id: {
      type: DataTypes.INTEGER,
    },
    grupo_ent: {
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "medicamento_ent",
    timestamps: false,
    freezeTableName: true,
  }
);

medicamentoEnt.belongsTo(Medicamento, { foreignKey: "medicamento_id" });
medicamentoEnt.belongsTo(Variable, { foreignKey: "grupo_ent" });

Variable.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  delete values.estado_id;
  return values;
};

module.exports = {
  Variable,
};
