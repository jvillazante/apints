const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");
const { Cie10 } = require("./cie10");
const { Paciente } = require("./paciente");
const { Variable } = require("./variable");

class Diagnostico extends Model {}

Diagnostico.init(
  {
    paciente_cronico_id: {
      type: DataTypes.INTEGER,
    },
    persona_id: {
      type: DataTypes.INTEGER,
    },
    variable_id: {
      type: DataTypes.INTEGER,
    },
    diagnostico_id: {
      type: DataTypes.INTEGER,
    },
    fecha_registro: {
      type: DataTypes.DATEONLY,
    },
    fecha_eliminacion: {
      type: DataTypes.DATEONLY,
    },
    estado_id: {
      type: DataTypes.INTEGER,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    sedes_id: {
      type: DataTypes.INTEGER,
    },
    red_id: {
      type: DataTypes.INTEGER,
    },
    municipio_id: {
      type: DataTypes.INTEGER,
    },
    establecimiento_id: {
      type: DataTypes.INTEGER,
    },
    tipo_registro: {
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "diagnosticos_cronicos",
    timestamps: false,
  }
);

Diagnostico.belongsTo(Variable, { foreignKey: "variable_id" });
Diagnostico.belongsTo(Cie10, { foreignKey: "diagnostico_id" });
Diagnostico.belongsTo(Paciente, { foreignKey: "paciente_cronico_id" });

Diagnostico.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  delete values.estado_id;
  return values;
};

// export default Usuario;
module.exports = {
  Diagnostico,
};
