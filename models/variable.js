const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");

class Variable extends Model {}

Variable.init(
  {
    // id: {
    //   type: DataTypes.INTEGER,
    //   primaryKey: true,
    // },
    descripcion: {
      type: DataTypes.STRING,
    },
    estado_id: {
      type: DataTypes.BOOLEAN,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "variables",
    timestamps: false,
    freezeTableName: true,
  }
);

Variable.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  delete values.estado_id;
  return values;
};

module.exports = {
  Variable,
};
