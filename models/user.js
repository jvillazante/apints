const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");

class User extends Model {}

User.init(
  {
    username: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,
    },
    departamento_id: {
      type: DataTypes.INTEGER,
    },
    red_id: {
      type: DataTypes.INTEGER,
    },
    municipio_id: {
      type: DataTypes.INTEGER,
    },
    establecimiento_id: {
      type: DataTypes.INTEGER,
    },
    status: {
      type: DataTypes.BOOLEAN,
    },
  },
  { sequelize: dbConexion, modelName: "user", timestamps: false }
);

User.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.password;
  delete values.id;
  return values;
};

module.exports = {
  User,
};
