const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");

class Cie10 extends Model {}

Cie10.init(
  {
    cie_alfa: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    cie_descripcion: {
      type: DataTypes.STRING,
    },
    cie_grupo: {
      type: DataTypes.INTEGER,
    },
    cie_capitulo: {
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "snis_cie10",
    timestamps: false,
    freezeTableName: true,
  }
);

Cie10.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.cie_alfa = values.cie_alfa.trim();
  values.cie_descripcion = values.cie_descripcion.trim();
  return values;
};

module.exports = {
  Cie10,
};
