const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");

//en el modelo aumentar la columna tabla_id (INTEGER)

class Catalogo extends Model {}

Catalogo.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    catalogoid: {
      type: DataTypes.INTEGER,
    },
    catalogodescripcion: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "catalogosgenericos",
    timestamps: false,
    freezeTableName: true,
  }
);

Catalogo.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  delete values.catalogovigente;
  return values;
};

module.exports = {
  Catalogo,
};
