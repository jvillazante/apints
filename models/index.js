const Paciente = require("./paciente");
const User = require("./user");
const Diagnostico = require("./diagnostico");
const Server = require("./server");
const Variable = require("./variable");
const Cie10 = require("./cie10");

module.exports = {
  Paciente,
  User,
  Server,
  Diagnostico,
  Variable,
  Cie10,
};
