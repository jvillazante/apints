const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");
const { Paciente } = require("./paciente");

class Antecedente extends Model {}

Antecedente.init(
  {
    paciente_cronico_id: {
      type: DataTypes.INTEGER,
    },
    persona_id: {
      type: DataTypes.INTEGER,
    },
    actividad_fisica: {
      type: DataTypes.INTEGER,
    },
    tabaquismo: {
      type: DataTypes.INTEGER,
    },
    alcoholismo: {
      type: DataTypes.INTEGER,
    },
    talla: {
      type: DataTypes.DECIMAL,
    },
    peso: {
      type: DataTypes.DECIMAL,
    },
    sistolica: {
      type: DataTypes.DECIMAL,
    },
    diastolica: {
      type: DataTypes.DECIMAL,
    },
    examen_ojo: {
      type: DataTypes.INTEGER,
    },
    cintura: {
      type: DataTypes.INTEGER,
    },
    imc_valor: {
      type: DataTypes.DOUBLE,
    },
    imc_descripcion: {
      type: DataTypes.STRING,
    },
    fecha_registro: {
      type: DataTypes.DATE,
    },
    user_id: {
      type: DataTypes.INTEGER,
    },
    establecimiento_id: {
      type: DataTypes.INTEGER,
    },
    frutas_verduras: {
      type: DataTypes.INTEGER,
    },
    sal: {
      type: DataTypes.INTEGER,
    },
    alimentos_procesados: {
      type: DataTypes.INTEGER,
    },
    bebidas_azucaradas: {
      type: DataTypes.INTEGER,
    },
    examen_pie: {
      type: DataTypes.INTEGER,
    },
    estado_id: {
      type: DataTypes.INTEGER,
    },
    tipo_g: {
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "antecedentes",
    timestamps: false,
    freezeTableName: true,
  }
);

Antecedente.belongsTo(Paciente, { foreignKey: "paciente_cronico_id" });

Antecedente.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  delete values.estado_id;
  return values;
};

module.exports = {
  Antecedente,
};
