const { Model, DataTypes } = require("sequelize");
const { dbConexion } = require("../db/connection");

class Laboratorio extends Model {}

Laboratorio.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: DataTypes.STRING,
    },
    unidad: {
      type: DataTypes.STRING,
    },
    tipo: {
      type: DataTypes.INTEGER,
    },
    tipo_descripcion: {
      type: DataTypes.STRING,
    },
    minimo: {
      type: DataTypes.INTEGER,
    },
    maximo: {
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize: dbConexion,
    modelName: "laboratorios",
    timestamps: false,
    freezeTableName: true,
  }
);

Laboratorio.prototype.toJSON = function () {
  var values = Object.assign({}, this.get());
  values.uid = values.id;
  delete values.id;
  return values;
};

module.exports = {
  Laboratorio,
};
